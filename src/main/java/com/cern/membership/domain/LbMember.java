package com.cern.membership.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class LbMember extends Member {

    private Boolean isAuthor;
    private Boolean countMnO;


    public LbMember(Long CCID, String firstname, String lastname, String email, Boolean isAuthor, Boolean countMnO) {
        super(CCID, firstname, lastname, email);
        this.isAuthor = isAuthor;
        this.countMnO = countMnO;
    }

    public Boolean getAuthor() {
        return isAuthor;
    }

    public void setAuthor(Boolean author) {
        isAuthor = author;
    }

    public Boolean getCountMnO() {
        return countMnO;
    }

    public void setCountMnO(Boolean countMnO) {
        this.countMnO = countMnO;
    }

    @Override
    public String toString() {
        return "LbMemberDto{" +
                "isAuthor=" + isAuthor +
                ", countMnO=" + countMnO +
                "} " + super.toString();
    }
}
