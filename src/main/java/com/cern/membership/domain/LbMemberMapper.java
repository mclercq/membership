package com.cern.membership.domain;

import com.cern.membership.model.LbMemberPersiste;
import org.springframework.stereotype.Component;

@Component
public class LbMemberMapper {

    /**
     * Coonvert a model Member as a MemberDto
     * @param lbMemberPersiste
     * @return
     */
    public static LbMember toLbMember(LbMemberPersiste lbMemberPersiste) {
        LbMember lbmember = new LbMember(
                lbMemberPersiste.getCCID(),
                lbMemberPersiste.getFirstname(),
                lbMemberPersiste.getLastname(),
                lbMemberPersiste.getEmail(),
                lbMemberPersiste.getAuthor(),
                lbMemberPersiste.getCountMnO()
        );
        return lbmember;
    }

    public static LbMemberPersiste toLbMemberPersiste(LbMember lbmember) {
        LbMemberPersiste lbMemberPersiste = new LbMemberPersiste();
        lbMemberPersiste.setEmail(lbmember.getEmail());
        lbMemberPersiste.setCCID(lbmember.getCCID());
        lbMemberPersiste.setFirstname(lbmember.getFirstname());
        lbMemberPersiste.setLastname(lbmember.getLastname());
        lbMemberPersiste.setAuthor(lbmember.getAuthor());
        lbMemberPersiste.setCountMnO(lbmember.getCountMnO());
        return lbMemberPersiste;
    }
}
