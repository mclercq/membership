package com.cern.membership.domain;

import com.cern.membership.model.MemberPersiste;
import org.springframework.stereotype.Component;

@Component
public class MemberMapper {

    /**
     * Coonvert a model Member as a MemberDto
     * @param memberPersiste
     * @return
     */
    public static Member toMember(MemberPersiste memberPersiste) {
        Member member = new Member(
                memberPersiste.getCCID(),
                memberPersiste.getFirstname(),
                memberPersiste.getLastname(),
                memberPersiste.getEmail());
        return member;
    }

    public static MemberPersiste toMemberPersiste(Member member) {
        MemberPersiste memberPersiste = new MemberPersiste(
                member.getCCID(),
                member.getFirstname(),
                member.getLastname(),
                member.getEmail()
        );
        return memberPersiste;
    }
}
