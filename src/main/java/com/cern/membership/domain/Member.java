package com.cern.membership.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


//@JsonInclude(value = JsonInclude.Include.NON_NULL)
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Member {
    private Long CCID;
    private String firstname;
    private String lastname;
    private String email;

    public Member(Long CCID, String firstname, String lastname, String email) {
        this.CCID = CCID;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public Long getCCID() {
        return CCID;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return firstname != null ? firstname.concat(" ").concat(lastname) : "";
    }


    @Override
    public String toString() {
        return "MemberDto{" +
                "CCID=" + CCID +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    //todo : add List of Institute + Employment



}


