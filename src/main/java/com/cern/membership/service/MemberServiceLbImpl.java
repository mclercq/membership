package com.cern.membership.service;

import com.cern.membership.controller.MemberController;
import com.cern.membership.domain.LbMember;
import com.cern.membership.domain.LbMemberMapper;
import com.cern.membership.exception.MemberNotFoundException;
import com.cern.membership.model.LbMemberPersiste;
import com.cern.membership.repository.LbMemberRepository;
import com.cern.membership.util.LbMemberModelAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class MemberServiceLbImpl implements MemberService<LbMember> {
    private LbMemberRepository lbmemberRepository;

    private LbMemberModelAssembler assembler;

    @Autowired
    public MemberServiceLbImpl(LbMemberRepository lbmemberRepository, LbMemberModelAssembler assembler) {
        this.lbmemberRepository = lbmemberRepository;
        this.assembler = assembler;
    }

    @Override
    public LbMember setDeceased(LbMember memberDto, Calendar deceasedDate) {
        return null;
    }

    @Override
    public CollectionModel<EntityModel<LbMember>> all() {
        List<EntityModel<LbMember>> lbmembers = lbmemberRepository.
                findAll()
                .stream()
                .map(LbMemberMapper::toLbMember)
                .map(assembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(lbmembers,
                linkTo(methodOn(MemberController.class).all()).withSelfRel());
    }

    @Override
    public ResponseEntity<?> newMember(LbMember newMember) {
        EntityModel<LbMember> entityModel = assembler.toModel(LbMemberMapper.toLbMember(lbmemberRepository.save(LbMemberMapper.toLbMemberPersiste(newMember))));

        return  ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @Override
    public EntityModel<LbMember> one(Long ccid) {
        LbMemberPersiste member = lbmemberRepository.findById(ccid).orElseThrow(() -> new MemberNotFoundException(ccid));

        return assembler.toModel(LbMemberMapper.toLbMember(member));

    }

    @Override
    public ResponseEntity<?> replaceMember(LbMember newMember, Long ccid) {
        LbMemberPersiste updatedLbMemberPersiste = lbmemberRepository.findById(ccid)
                .map(lbMember -> {
                    lbMember.setFirstname(newMember.getFirstname());
                    lbMember.setLastname(newMember.getLastname());
                    lbMember.setEmail(newMember.getEmail());
                    lbMember.setAuthor(newMember.getAuthor());
                    lbMember.setCountMnO(newMember.getCountMnO());
                    return lbmemberRepository.save(lbMember);
                })
                .orElseGet(() -> {
                    return lbmemberRepository.save(LbMemberMapper.toLbMemberPersiste(newMember));
                });

        EntityModel<LbMember> entityModel = assembler.toModel(LbMemberMapper.toLbMember(updatedLbMemberPersiste));

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);

    }

    @Override
    public ResponseEntity<?> deleteMember(Long ccid) {
        lbmemberRepository.deleteById(ccid);

        return ResponseEntity.noContent().build();

    }
}
