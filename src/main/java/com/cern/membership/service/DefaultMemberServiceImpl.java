package com.cern.membership.service;

import com.cern.membership.controller.MemberController;
import com.cern.membership.domain.Member;
import com.cern.membership.domain.MemberMapper;
import com.cern.membership.exception.MemberNotFoundException;
import com.cern.membership.model.MemberPersiste;
import com.cern.membership.repository.MemberRepository;
import com.cern.membership.util.MemberModelAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DefaultMemberServiceImpl implements MemberService<Member> {
    protected final MemberRepository<MemberPersiste> memberRepository;

    protected final MemberModelAssembler assembler;

    @Autowired
    public DefaultMemberServiceImpl(MemberRepository memberRepository, MemberModelAssembler assembler) {
        this.memberRepository = memberRepository;
        this.assembler = assembler;
    }

    public Member setDeceased(Member member, Calendar deceasedDate) {
        return null;
    }

    @Override
    public CollectionModel<EntityModel<Member>> all() {
        List<EntityModel<Member>> members = memberRepository
                .findAll()
                .stream()
                .map(MemberMapper::toMember)
                .map(assembler::toModel)
                .collect(Collectors.toList());
        return CollectionModel.of(members,
                linkTo(methodOn(MemberController.class)
                        .all())
                        .withSelfRel());
    }

    @Override
    public ResponseEntity<?> newMember(Member newMember) {
        EntityModel<Member> entityModel = assembler.toModel(MemberMapper.toMember(memberRepository.save(MemberMapper.toMemberPersiste(newMember))));
        return  ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @Override
    public EntityModel<Member> one(Long ccid) {
        MemberPersiste memberPersiste = memberRepository.findById(ccid).orElseThrow(() -> new MemberNotFoundException(ccid));
        return assembler.toModel(MemberMapper.toMember(memberPersiste));
    }

    @Override
    public ResponseEntity<?> replaceMember(Member newMember, Long ccid) {
        MemberPersiste updatedMemberPersiste = memberRepository.findById(ccid)
                .map(Member -> {
                    Member.setFirstname(newMember.getFirstname());
                    Member.setLastname(newMember.getLastname());
                    Member.setEmail(newMember.getEmail());
                    return memberRepository.save(Member);
                })
                .orElseGet(() -> {
                    return memberRepository.save(MemberMapper.toMemberPersiste(newMember));
                });
        EntityModel<Member> entityModel = assembler.toModel(MemberMapper.toMember(updatedMemberPersiste));
        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @Override
    public ResponseEntity<?> deleteMember(Long ccid) {
        memberRepository.deleteById(ccid);
        return ResponseEntity.noContent().build();
    }
}
