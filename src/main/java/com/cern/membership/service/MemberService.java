package com.cern.membership.service;

import com.cern.membership.domain.Member;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import java.util.Calendar;


public interface MemberService<T extends Member> {
    T setDeceased(T member, Calendar deceasedDate);
    CollectionModel<EntityModel<T>> all();
    ResponseEntity<?> newMember(T newMember);
    EntityModel<T> one(Long ccid);
    ResponseEntity<?> replaceMember(T newMember, Long ccid);
    ResponseEntity<?> deleteMember(Long ccid);
}
