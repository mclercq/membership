package com.cern.membership.service;

import com.cern.membership.domain.LbMember;

public interface LbMemberService extends MemberService<LbMember> {

    Boolean isAuthor(Long ccid);
}
