package com.cern.membership.controller;

import com.cern.membership.domain.LbMember;
import com.cern.membership.domain.Member;
import com.cern.membership.service.LbMemberService;
import com.cern.membership.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
//@RequestMapping("/commun")
public class MemberController<T extends Member> {

//    @Qualifier("lbMemberServiceImpl")
    @Qualifier("defaultMemberServiceImpl")
    @Autowired
//    protected LbMemberService memberService;
    protected MemberService memberService;

    @GetMapping("/members")
    public CollectionModel<EntityModel<?>> all() {
        return memberService.all();
    }

    @PostMapping("/members")
    ResponseEntity<?> newMember(@RequestBody T newMember) {
        return memberService.newMember(newMember);
    }

    @GetMapping("/members/{ccid}")
    public EntityModel<?> one(@PathVariable Long ccid) {
        return memberService.one(ccid);
    }

    @PutMapping("/members/{ccid}")
    ResponseEntity<?> replaceMember(@RequestBody T newMember, @PathVariable Long ccid) {
        return memberService.replaceMember(newMember, ccid);
    }

    @DeleteMapping("/members/{ccid}")
    ResponseEntity<?> deleteMember(@PathVariable Long ccid) {
        return memberService.deleteMember(ccid);
    }

}
