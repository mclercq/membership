package com.cern.membership.controller;

import com.cern.membership.domain.LbMember;
import com.cern.membership.domain.Member;
import com.cern.membership.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@RestController
//@RequestMapping("/lhcb")
public class LbMemberController {

    // todo:
    //  * Pour les routes propre a LHCb, necessaire dans ce Framework ?
    //  * Est-ce que LHCb aura ses propres routes ?

    @Qualifier("lbMemberServiceImpl")
    @Autowired
    protected MemberService memberService;

    @GetMapping("/members")
    public CollectionModel<EntityModel<LbMember>> all() {
        return memberService.all();
    }

    @GetMapping("/members/{ccid}")
    public EntityModel<Member> one(@PathVariable Long ccid) {
        return memberService.one(ccid);
    }

    @PutMapping("/members/{ccid}")
    ResponseEntity<?> replaceMember(@RequestBody Member newMember, @PathVariable Long ccid) {
        return memberService.replaceMember(newMember, ccid);
    }

    @DeleteMapping("/members/{ccid}")
    ResponseEntity<?> deleteMember(@PathVariable Long ccid) {
        return memberService.deleteMember(ccid);
    }

    @PostMapping("/members")
    ResponseEntity<?> newMember(@RequestBody LbMember newMember) {
        return memberService.newMember(newMember);
    }

}
