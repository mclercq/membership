package com.cern.membership.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class MemberPersiste {

    @Id
    protected Long CCID;
    protected String firstname;
    protected String lastname;
    protected String email;

    public MemberPersiste() {
    }

    public MemberPersiste(Long CCID, String firstname, String lastname, String email) {
        this.CCID = CCID;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public Long getCCID() {
        return CCID;
    }

    public void setCCID(Long CCID) {
        this.CCID = CCID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "Member{" +
                "CCID=" + CCID +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
