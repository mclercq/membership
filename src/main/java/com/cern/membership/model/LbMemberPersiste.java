package com.cern.membership.model;

import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class LbMemberPersiste extends MemberPersiste {

    private Boolean isAuthor;
    private Boolean countMnO;

    public LbMemberPersiste() {
    }

    public LbMemberPersiste(Long CCID, String firstname, String lastname, String email, Boolean isAuthor, Boolean countMnO) {
        super(CCID, firstname, lastname, email);
        this.isAuthor = isAuthor;
        this.countMnO = countMnO;
    }

    public Boolean getAuthor() {
        return isAuthor;
    }

    public void setAuthor(Boolean author) {
        isAuthor = author;
    }

    public Boolean getCountMnO() {
        return countMnO;
    }

    public void setCountMnO(Boolean countMnO) {
        this.countMnO = countMnO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LbMemberPersiste lbMemberPersiste = (LbMemberPersiste) o;
        return Objects.equals(isAuthor, lbMemberPersiste.isAuthor) &&
                Objects.equals(countMnO, lbMemberPersiste.countMnO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isAuthor, countMnO);
    }

    @Override
    public String toString() {
        return "LbMember{" +
                "isAuthor=" + isAuthor +
                ", countMnO=" + countMnO +
                ", CCID=" + CCID +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
