package com.cern.membership.repository;

import com.cern.membership.model.LbMemberPersiste;

public interface LbMemberRepository extends MemberRepository<LbMemberPersiste> {
}
