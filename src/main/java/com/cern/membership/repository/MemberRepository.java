package com.cern.membership.repository;


import com.cern.membership.model.MemberPersiste;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository<T extends MemberPersiste> extends JpaRepository<T, Long> {

}
