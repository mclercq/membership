package com.cern.membership.exception;

public class MemberNotFoundException extends RuntimeException{

    public MemberNotFoundException(Long ccid) {
        super("Could not find member " + ccid);
    }
}
