package com.cern.membership;

import com.cern.membership.model.LbMemberPersiste;
import com.cern.membership.model.MemberPersiste;
import com.cern.membership.repository.LbMemberRepository;
import com.cern.membership.repository.MemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseLoader {

    private static final Logger log = LoggerFactory.getLogger(DatabaseLoader.class);

    @Bean
    CommandLineRunner initDatabase(MemberRepository memberRepository, LbMemberRepository lbMemberRepository) {
        return args -> {
            memberRepository.save(new MemberPersiste(1L, "Camille", "Eva", "cam.eva@ge.ch"));
            memberRepository.save(new MemberPersiste(2L, "Alexandre", "Clercq", "alex.c@lyon.fr"));
            lbMemberRepository.save(new LbMemberPersiste(4L, "Maxime", "Clercq", "m.c@hepia.ch", false, false));
            lbMemberRepository.save(new LbMemberPersiste(5L, "Yoan", "Tempest", "yoan@tempest.com", true, true));

            memberRepository.findAll().forEach(member -> log.info("Preloaded" + member));
        };
    }
}
