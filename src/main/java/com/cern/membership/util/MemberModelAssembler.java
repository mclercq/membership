package com.cern.membership.util;

import com.cern.membership.controller.MemberController;
import org.springframework.hateoas.EntityModel;
import com.cern.membership.domain.Member;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class MemberModelAssembler implements RepresentationModelAssembler<Member, EntityModel<Member>> {

    @Override
    public EntityModel<Member> toModel(Member member) {

        return EntityModel.of(member,
                linkTo(methodOn(MemberController.class).one(member.getCCID())).withSelfRel(),
                linkTo(methodOn(MemberController.class).all()).withRel("members"));
    }
}
