package com.cern.membership.util;

import com.cern.membership.controller.MemberController;
import com.cern.membership.domain.LbMember;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class LbMemberModelAssembler implements RepresentationModelAssembler<LbMember, EntityModel<LbMember>> {

    @Override
    public EntityModel<LbMember> toModel(LbMember member) {

        return EntityModel.of(member,
                linkTo(methodOn(MemberController.class).one(member.getCCID())).withSelfRel(),
                linkTo(methodOn(MemberController.class).all()).withRel("members"));
    }
}